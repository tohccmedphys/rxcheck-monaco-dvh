
from rxcheck_base_importer import BaseImporter

import dvh
import numpy
import pandas
import os
import re
import sys

from . import settings

HEADER_RE = re.compile(
    """
    ^Patient\sID:\s(?P<patient_id>[~0-9a-zA-Z_+]+).+
    Plan\sName:\s(?P<plan_id>[~0-9a-zA-Z_+()]+).+
    Resolution:\s(?P<resolution>[0-9.]+)\((?P<resolution_unit>[a-zA-Z]+)\).+
    Bin\sWidth:\s(?P<bin_width>[0-9.]+)\((?P<bin_width_unit>[a-zA-Z]+)\).+
    Dose\s\w+:\s(?P<dose_unit>[a-zA-Z]+)\s.+
    Volume\s\w+:\s(?P<volume_unit>[a-zA-Z%]+.?).*
    """,
    re.VERBOSE,
)


def absolute_file_paths(directory):
    dirpath, _, filenames = next(os.walk(directory))
    for f in filenames:
        yield os.path.abspath(os.path.join(dirpath, f))


class MonacoDVHImporter(BaseImporter):

    NAME = 'Monaco DVH'
    HANDLE = 'monacodvh'
    MAX_AT_ZERO_VOL = False

    def __init__(self, *args, **kwargs):

        self.import_dir = kwargs.pop('dvh_directory', settings.MONACO_DVH_DIRECTORY)

        super(MonacoDVHImporter, self).__init__(*args, **kwargs)

    def get_patients(self):

        paths = absolute_file_paths(self.import_dir)

        patients = {}
        for path in paths:
            try:
                try:
                    f = open(path, 'r')
                    header_info = HEADER_RE.match(f.readline()).groupdict()
                except AttributeError:
                    continue

                error = ''

                try:
                    dvh_filename = os.path.basename(path)
                except Exception as e:
                    dvh_filename = 'dvh loading error'
                    error = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)

                try:
                    patient_id = header_info['patient_id']
                    patient_id = self.clean_patient_id(patient_id)
                except Exception as e:
                    patient_id = 'patient_id error'
                    error = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)

                try:
                    plan_id = header_info['plan_id']
                except Exception as e:
                    plan_id = 'plan_id error'
                    error = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)

                verbose_name = plan_id + '__(' + dvh_filename.split('.txt')[0] + ')'
                plan = {
                    'plan_id': plan_id,
                    'name': plan_id,
                    'dvh_filename': dvh_filename,
                    'verbose_name': verbose_name,
                    'multiple_with_plan_id': False,
                    'error': error
                }

                if patient_id in patients:

                    found_plan_verbose = False
                    for vp in patients[patient_id]:
                        if patients[patient_id][vp]['name'] == plan_id:
                            found_plan_verbose = True
                            patients[patient_id][vp]['multiple_with_plan_id'] = True

                    if found_plan_verbose:
                        plan['multiple_with_plan_id'] = True

                    patients[patient_id][verbose_name] = plan
                else:
                    patients[patient_id] = {
                        verbose_name: plan,
                    }

            except Exception as e:
                plan_e = plan
                plan_e['error'] = '%s on (%s) message: %s' % (type(e), sys.exc_info()[-1].tb_lineno, e)
                if patient_id in patients:
                    patients[patient_id][verbose_name] = plan_e
                else:
                    patients[patient_id] = {verbose_name: plan_e}

        return patients

    def get_patient_plan(self, patient_id, plan_id, dvh_filename):
        path = self.find_dvh_path(patient_id, plan_id, dvh_filename)
        if path:
            return self.get_plan_data(path)

    def clean_patient_id(self, patient_id):
        if '~' in patient_id:
            patient_id = patient_id.split('~')[-1]
        return patient_id

    def find_dvh_path(self, patient_id, plan_id, dvh_filename):
        """return path of the DVH file corresponding to the input patient_id & plan_id
        This just naively opens every single file in the MONACO_DVH_DIRECTORY and tries
        to match the header data looking for the correct patient/plan.
        """

        path = self.import_dir + '\\' + dvh_filename

        f = open(path, 'r')
        header_info = HEADER_RE.match(f.readline()).groupdict()
        f.close()
        pid = self.clean_patient_id(header_info["patient_id"])
        if (pid, header_info["plan_id"]) == (patient_id, plan_id):
            return path

        raise ValueError("Unable to find valid DVH file {1} for plan {0} for patient {2}".format(dvh_filename, plan_id, patient_id))

    def get_plan_data(self, path):
        """return dvh data for input file path."""

        with open(path, 'r') as f:

            header_data = HEADER_RE.match(f.readline()).groupdict()
            if header_data['dose_unit'] == 'cGy':
                multiplier = 1.
            elif header_data['dose_unit'] == 'Gy':
                multiplier = 100.
            else:
                raise ValueError('Unknown dose unit')

            if 'cm' not in header_data['volume_unit']:
                raise ValueError('Invalid volume mode {0}. Must be absolute volume in cc\'s'.format(header_data['volume_unit']))

            skiprows = 4 if f.readline().startswith('English') else 3

            f.seek(0)
            try:
                data_frame = pandas.read_csv(f, engine='python', sep='\s{4,}', skiprows=skiprows, skipfooter=3, names=['struct', 'dose', 'volume'])
            except pandas.errors.ParserError as e:
                if 'Expected 3 fields in line ' in str(e):
                    line_num = str(e).replace('Expected 3 fields in line ', '').split(',')[0]
                    raise pandas.errors.ParserError("Can not read dvh file. Too many consecutive spaces in structure name on line {}?".format(line_num))
                else:
                    raise e
            data_frame.dose *= multiplier

            bin_width = float(header_data['bin_width']) * multiplier

            plan = {
                'name': header_data['plan_id'],
                'id': header_data['plan_id'],
                'dvh_filename': os.path.basename(path),
                'structures': self.structures_from_data_frame(data_frame, bin_width)
            }

        return plan

    def structures_from_data_frame(self, data_frame, bin_width):

        grouped = data_frame.groupby('struct')
        dvh_volumes = grouped.volume
        dvh_doses = grouped.dose

        structures = []
        for name in grouped.groups.keys():

            volumes = numpy.array(dvh_volumes.get_group(name), dtype=float)

            if not dvh.monotonic_decreasing(volumes):
                raise ValueError('DVH must be exported as a cumulative dvh with absolute volume')

            struct_volume = volumes.max()

            doses = numpy.array(dvh_doses.get_group(name))
            struct_dvh = dvh.DVH(doses, volumes, self.MAX_AT_ZERO_VOL)

            structures.append({
                'name': name.replace("'", ""),
                'volume': struct_volume,
                'dvh': {
                    'doses': struct_dvh.doses.tolist(),
                    'volumes': struct_dvh.cum_volumes.tolist(),
                },
            })

        return structures
